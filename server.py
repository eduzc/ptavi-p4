#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""Primero inicializamos server."""

"""Clase (y programa principal) para un servidor de eco en UDP simple."""

import socketserver
import sys
import time
import json


class SIPRegisterHandler(socketserver.DatagramRequestHandler):
    """Inicializo el dictionario de usuarios."""

    dict = {}
    users_to_delete = []

    def json2register(self):
        """
        Metodo : 
	Descargo fichero json en el dictionario.
        """
        try:
            with open('registered.json', 'r') as jsonfile:
                self.dict = json.load(jsonfile)
        except:
            pass

    def register2json(self):
        """
	Metodo : 
        Escribo dictionario.
        En formato json en el fichero registered.json.
        """
        with open('registered.json', 'w') as jsonfile:
            json.dump(self.dict, jsonfile, indent=4)

    def handle(self):
        """
	Metodo:
        Recibo el mensaje del cliente y lo decodifico.
        Si la petition es un register guardo
        la informacion en un dictionario.
        """
        self.json2register()
        message = ''

        for line in self.rfile:
            message += line.decode('utf-8')

        print("El cliente nos manda ", message)

        if message != '\r\n':
            """Comprobamos el formato del mensaje y si la peticion es register."""

            petition, address, sip, expires = message.split() #split
            if petition == 'REGISTER':
                user = address.split(':')[1]
                IP = self.client_address[0]
                expired_time = time.time() + int(expires)
                """Mostramos la fecha y la hora."""
                time_exp = time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime(expired_time))
                now = time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime(time.time()))

                self.dict[user] = {'address': IP, 'expires': time_exp}

                for user in self.dict:
                    if now >= self.dict[user]['expires']:
                        self.users_to_delete.append(user)

                for user in self.users_to_delete:
                    del self.dict[user]

                self.register2json()

                print(self.dict)
        self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")



if __name__ == "__main__":
    """Escuchamos en el puerto dado por el usuario
     llamamos a EchoHandler para llevar a cabo la peticion."""
    serv = socketserver.UDPServer(('', int(sys.argv[1])), SIPRegisterHandler)

    print("Lanzando servidor UDP de eco...")
    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
