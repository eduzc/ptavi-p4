#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa cliente UDP que abre un socket a un servidor
"""

"""Iniciamos primero el servidor, luego el cliente
"""

import socket
import sys
try:
    """Constantes. Direccion IP del servidor y contenido a enviar"""
    SERVER = sys.argv[1]
    PORT = int(sys.argv[2])
    PETITION = sys.argv[3] #cambiamos la frase por una peticion
    ADDRESS = sys.argv[4]
    EXPIRES = sys.argv[5]
except IndexError:
    sys.exit("error")




with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
   message = PETITION.upper() + ' sip:' + ADDRESS + ' SIP/2.0 ' + EXPIRES
   my_socket.connect((SERVER, PORT))
   print("Enviando peticion:", PETITION.upper() + "con direccion:", ADDRESS + "Server :", SERVER + "numero de puerto :", PORT)
   my_socket.send(bytes(message, 'utf-8') + b'\r\n') #cambio la linea de mensaje por message al utilizar sip
   data = my_socket.recv(1024)
   print('Recibido -- ', data.decode('utf-8'))

print("Socket terminado.")

